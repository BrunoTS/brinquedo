#include "Laser.h"

Laser::Laser(byte pin) {
  this->pin = pin;
  init();
}

void Laser::init() {
  pinMode(pin, OUTPUT);
  off();
}

void Laser::on() {
  digitalWrite(pin, HIGH);
}

void Laser::off() {
  digitalWrite(pin, LOW);
}
