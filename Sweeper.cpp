#include "Sweeper.h"

Sweeper::Sweeper(int updateInterval) {
  this->updateInterval = updateInterval;
}

void Sweeper::setInterval(int interval) {
  this->updateInterval = interval;
}

void Sweeper::setIncrement(int increment) {
  this->increment = increment;
}

void Sweeper::attach(int pin) {
  servo.attach(pin);
}

void Sweeper::detach() {
  servo.detach();
}

void Sweeper::update() {
  if ((moving) && (millis() - lastUpdate) > updateInterval) {
    lastUpdate = millis();
    if (forward) {
      if (curPos >= newPos) {
        moving = false;
      } else {
        curPos = curPos + increment;
      }
    } else {
      if (curPos <= newPos) {
        moving = false;
      } else {
        curPos = curPos - increment;
      }
    }
    if (moving) {
      servo.write(curPos);
    }
  }
}

void Sweeper::move(int pos) {
  oldPos = curPos;
  newPos = pos;
  moving = oldPos != newPos;
  forward = oldPos < newPos;
}

void Sweeper::moveInstantly(int pos) {
  curPos = pos;
  servo.write(curPos);
  moving = false;
}

boolean Sweeper::isMoving() {
  return moving;
}

int Sweeper::getPos() {
  return curPos;
}
