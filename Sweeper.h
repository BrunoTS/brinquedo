#ifndef SWEEPER_H
#define SWEEPER_H

#include <Arduino.h>
#include <Servo.h>

class Sweeper {
  private:
    Servo servo;
    int  updateInterval;
    unsigned long lastUpdate;
    int curPos;
    int oldPos;
    int newPos;
    int increment = 1;
    boolean moving = false;
    boolean forward = true;

  public:
    Sweeper(int interval);
    void attach(int pin);
    void detach();
    void update();
    void move(int pos);
    void setInterval(int interval);
    void setIncrement(int increment);
    void moveInstantly(int pos);
    int getPos();
    boolean isMoving();
};

#endif
