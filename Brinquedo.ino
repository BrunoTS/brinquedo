#include "StatusLED.h"
#include "Sweeper.h"
#include "Laser.h"
#include "Ultrasonic.h"

#define PIN_LASER 2
#define PIN_RGBLED_R 3
#define PIN_BUZZER 4
#define PIN_RGBLED_G 5
#define PIN_RGBLED_B 6
#define PIN_TRIGGER 7
#define PIN_ECHO 8
#define PIN_SERVO_H 9
#define PIN_SERVO_V 10

// Definição dos pinos analógicos
const int pinoPotenciometro1 = A0;
const int pinoPotenciometro2 = A1;
const int pinoPotenciometro3 = A2;

enum LedStatus {NULO, ESPERAR, PREPARAR, FUNCIONAR};

Sweeper servoH(1);
Sweeper servoV(1);
StatusLED led(PIN_RGBLED_R, PIN_RGBLED_G, PIN_RGBLED_B);
Laser laser(PIN_LASER);
Ultrasonic ultrasonic(PIN_TRIGGER, PIN_ECHO);
LedStatus lastLedStatus;

long tempoParaAcionamento = 0;
long tempoAcionado = 0;
long distanciaDeAcionamento = 0;
bool funcionando = false;
long primeiraDeteccao = 0;
bool primeiroAvisoAcionado = false;
bool segundoAvisoAcionado = false;
long inicio = 0;
long ultimoMovimentoDoLaser = 0;
long proximaSaidaSerial = 0;
long tempoTotal = 0;
int tempoParaMoverNovamente = 0;

void setup() {
  // Configurando Speaker como saida
  pinMode(PIN_BUZZER, OUTPUT);
  digitalWrite(PIN_BUZZER, LOW);

  // Vinculando pinos dos servo motores
  servoH.attach(PIN_SERVO_H);
  servoV.attach(PIN_SERVO_V);

  // Configurando pinos dos potenciometros como entrada
  pinMode(pinoPotenciometro1, INPUT);
  pinMode(pinoPotenciometro2, INPUT);
  pinMode(pinoPotenciometro3, INPUT);

  // Iniciando porta serial para saida
  Serial.begin(9600);

  /*********************************************/
  /* === Iniciando testes dos dispositivos === */
  /*********************************************/
  Serial.println("Iniciando testes");
  delay(1000);
  /**********/
  /* LASER */
  /**********/
  laser.on();
  delay(1000);
  laser.off();
  /**********/
  /* BUZZER */
  /**********/
  tone(PIN_BUZZER, 1000, 500);
  /**********************/
  /* SENSOR ULTRASONICO */
  /**********************/
  Serial.println(ultrasonic.cm());
  /***********/
  /* RGB LED */
  /***********/
  led.RGB(255, 0, 0);
  delay(1000);
  led.RGB(0, 255, 0);
  delay(1000);
  led.RGB(0, 0, 255);
  delay(1000);
  led.RGB(0, 0, 0);
  /*****************/
  /* SERVO-MOTORES */
  /*****************/
  servoH.moveInstantly(0);
  delay(500);
  servoH.moveInstantly(180);
  delay(500);
  servoH.moveInstantly(90);
  delay(500);
  servoV.moveInstantly(90);
  delay(500);
  servoV.moveInstantly(110);
  delay(500);
  servoV.moveInstantly(90);

  Serial.println("Finalizado testes.");
  delay(1000);
}

void loop() {
  lerPotenciomentros();

  if (!funcionando) {
    aguardando();
  } else {
    long tempoRestante = (tempoTotal - (millis() - inicio));
    if (tempoRestante <= 0) {
      parar();
    } else {
      moverLaser();
    }
  }

  updateAll();
}

void moverLaser() {
  if (servoH.isMoving() || servoV.isMoving()) {
    ultimoMovimentoDoLaser = millis();
  } else {
    if (millis() - ultimoMovimentoDoLaser > (tempoParaMoverNovamente * 1000)) {
      int eixoH = random(45, 130);
      int eixoV = random(90, 100);
      int velocidade = random(5, 10);
      tempoParaMoverNovamente = random(3, 10);
      int incrementH = random(1, 5) * 2;
      int incrementV = random(1, 3);
      Serial.print("Movendo os servos para (");
      Serial.print(eixoH);
      Serial.print(",");
      Serial.print(eixoV);
      Serial.println(")");
      servoH.setIncrement(incrementH);
      servoH.setInterval(31 * (15 - velocidade));
      servoV.setIncrement(incrementV);
      servoV.setInterval(57 * (15 - velocidade));
      servoH.move(eixoH);
      servoV.move(eixoV);
      ultimoMovimentoDoLaser = millis();
    }
  }
}

void parar() {
  funcionando = false;
  cancelarAvisos();
  laser.off();
  esperar();
  servoH.move(90);
  servoV.move(90);
}

void lerPotenciomentros() {
  tempoParaAcionamento = map(analogRead(pinoPotenciometro1), 3, 1019, 1, 21) * 3;
  tempoAcionado = map(analogRead(pinoPotenciometro2), 3, 1019, 1, 13) * 300;
  distanciaDeAcionamento = map(analogRead(pinoPotenciometro3), 3, 1019, 10, 101);
}

void updateAll() {
  led.update();
  servoH.update();
  servoV.update();
}

void aguardando() {
  float distancia = ultrasonic.cm();
  if (distancia <= distanciaDeAcionamento) {
    preparar();
    if (primeiraDeteccao == 0) {
      primeiraDeteccao = millis();
    }
    if (!primeiroAvisoAcionado && (millis() - primeiraDeteccao) >= ((tempoParaAcionamento * 1000) / 3)) {
      primeiroAviso();
    }
    if (!segundoAvisoAcionado && (millis() - primeiraDeteccao) >= (((tempoParaAcionamento * 1000) / 3) * 2)) {
      segundoAviso();
    }
    if ((millis() - primeiraDeteccao) >= (tempoParaAcionamento * 1000)) {
      Serial.println("Começando \\o/");
      start();
    }
  } else {
    esperar();
    if (primeiraDeteccao != 0) {
      primeiraDeteccao = 0;
      cancelarAvisos();
    }
  }
}

void start() {
  funcionando = true;
  tempoTotal = tempoAcionado * 1000;
  inicio = millis();
  funcionar();
  laser.on();
  ultimoMovimentoDoLaser = 0;
  moverLaser();
}

void primeiroAviso() {
  tone(PIN_BUZZER, 1000, 500);
  primeiroAvisoAcionado = true;
}

void segundoAviso() {
  tone(PIN_BUZZER, 1500, 500);
  segundoAvisoAcionado = true;
}

void cancelarAvisos() {
  primeiroAvisoAcionado = false;
  segundoAvisoAcionado = false;
}

void esperar() {
  if (lastLedStatus != ESPERAR) {
    Serial.println("Mudando o status do led [ESPERAR]");
    lastLedStatus = ESPERAR;
    led.esperar();
  }
}

void preparar() {
  if (lastLedStatus != PREPARAR) {
    Serial.println("Mudando o status do led [PREPARAR]");
    lastLedStatus = PREPARAR;
    led.preparar();
  }
}

void funcionar() {
  if (lastLedStatus != FUNCIONAR) {
    Serial.println("Mudando o status do led [FUNCIONAR]");
    lastLedStatus = FUNCIONAR;
    led.funcionar();
  }
}
