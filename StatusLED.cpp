#include "StatusLED.h"

LedStep stepsEsperar[6] = {LedStep(2, 0, 2, 50),
                           LedStep(0, 0, 0, 100),
                           LedStep(2, 0, 2, 50),
                           LedStep(0, 0, 0, 100),
                           LedStep(2, 0, 2, 50),
                           LedStep(0, 0, 0, 750)
                          };
Vector<LedStep> vectorEsperar(stepsEsperar, 6);
LedStep stepsPreparar[10] = {LedStep(2, 2, 0, 100),
                             LedStep(4, 4, 0, 100),
                             LedStep(6, 6, 0, 100),
                             LedStep(8, 8, 0, 100),
                             LedStep(10, 10, 0, 100),
                             LedStep(8, 8, 0, 100),
                             LedStep(6, 6, 0, 100),
                             LedStep(4, 4, 0, 100),
                             LedStep(2, 2, 0, 100),
                             LedStep(0, 0, 0, 100)
                            };
Vector<LedStep> vectorPreparar(stepsPreparar, 10);
LedStep stepsFuncionar[20] = {LedStep(0, 2, 0, 100),
                     LedStep(0, 4, 0, 100),
                     LedStep(0, 6, 0, 100),
                     LedStep(0, 8, 0, 100),
                     LedStep(0, 10, 0, 100),
                     LedStep(0, 10, 2, 100),
                     LedStep(0, 10, 4, 100),
                     LedStep(0, 10, 6, 100),
                     LedStep(0, 10, 8, 100),
                     LedStep(0, 10, 10, 100),
                     LedStep(0, 8, 10, 100),
                     LedStep(0, 6, 10, 100),
                     LedStep(0, 4, 10, 100),
                     LedStep(0, 2, 10, 100),
                     LedStep(0, 0, 10, 100),
                     LedStep(0, 0, 8, 100),
                     LedStep(0, 0, 6, 100),
                     LedStep(0, 0, 4, 100),
                     LedStep(0, 0, 3, 100),
                     LedStep(0, 0, 0, 1000)
                    };
Vector<LedStep> vectorFuncionar(stepsFuncionar, 20);


StatusLED::StatusLED(int pinRed, int pinGreen, int pinBlue) {
  this->pinRed = pinRed;
  pinMode(pinRed, OUTPUT);
  this->pinGreen = pinGreen;
  pinMode(pinGreen, OUTPUT);
  this->pinBlue = pinBlue;
  pinMode(pinBlue, OUTPUT);
  RGB(0, 0, 0);
}

void StatusLED::reset() {
  RGB(0, 0, 0);
  pos = 0;
  nextUpdate = 0;
  update();
}

void StatusLED::update() {
  unsigned long currentMillis = millis();
  if (currentMillis >= nextUpdate) {
    if (steps.size() > 0) {
      if (steps.size() > pos) {
        Serial.println("Atualizando led pra próxima posição");
        int red = steps[pos].red;
        int green = steps[pos].green;
        int blue = steps[pos].blue;
        int duration = steps[pos].duration;
        Serial.print("RGB(");
        Serial.print(red);
        Serial.print(", ");
        Serial.print(green);
        Serial.print(", ");
        Serial.print(blue);
        Serial.print(") por ");
        Serial.print(duration);
        Serial.println("ms.");
        pos++;
        RGB(red, green, blue);
        nextUpdate = millis() + duration;
      } else {
        pos = 0;
        update();
      }
    }
  }
}

void StatusLED::setSteps(Vector<LedStep> steps) {
  this->steps = steps;
  reset();
}

void StatusLED::RGB(int redValue, int greenValue, int blueValue) {
  if (redValue > 255) {
    redValue = 255;
  }
  if (greenValue > 255) {
    greenValue = 255;
  }
  if (blueValue > 255) {
    blueValue = 255;
  }
  if (redValue < 0) {
    redValue = 0;
  }
  if (greenValue < 0) {
    greenValue = 0;
  }
  if (blueValue < 0) {
    blueValue = 0;
  }
  analogWrite(pinRed, 255 - redValue);
  analogWrite(pinGreen, 255 - greenValue);
  analogWrite(pinBlue, 255 - blueValue);
}

void StatusLED::esperar() {
  this->setSteps(vectorEsperar);
}
void StatusLED::preparar() {
  this->setSteps(vectorPreparar);
}
void StatusLED::funcionar() {
  this->setSteps(vectorFuncionar);
}
