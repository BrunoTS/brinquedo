#ifndef ULTRASONIC_H
#define ULTRASONIC_H

#include <Arduino.h>

class Ultrasonic {
  private:
    int pinTrigger;
    int pinEcho;
    void init();
    unsigned long lastMeasureValue;
    unsigned long nextMeasureTime;

  public:
    Ultrasonic(int pinTrigger, int pinEcho);
    long timeOfFlight();
    float cm();
};

#endif
