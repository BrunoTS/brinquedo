#include "Ultrasonic.h"

Ultrasonic::Ultrasonic(int pinTrigger, int pinEcho) {
  this->pinTrigger = pinTrigger;
  this->pinEcho = pinEcho;
  init();
}

void Ultrasonic::init() {
  pinMode(pinTrigger, OUTPUT);
  digitalWrite(pinTrigger, LOW);
  pinMode(pinEcho, INPUT);
}

long Ultrasonic::timeOfFlight() {
  if (millis() >= nextMeasureTime) {
    digitalWrite(pinTrigger, HIGH);
    delay(10);
    digitalWrite(pinTrigger, LOW);
    lastMeasureValue = pulseIn(pinEcho, HIGH);
    nextMeasureTime = millis() + 100;
  }
  return lastMeasureValue;
}

float Ultrasonic::cm() {
  float distancia = (timeOfFlight() / 2) * 0.0343;
  return distancia;
}
