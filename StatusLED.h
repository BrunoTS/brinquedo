#ifndef STATUS_LED_H
#define STATUS_LED_H

#include <Arduino.h>
#include "Vector.h"

struct LedStep {
  int red;
  int green;
  int blue;
  int duration;
  
  LedStep(int red, int green, int blue, long duration) {
    this->red = red;
    this->green = green;
    this->blue = blue;
    this->duration = duration;
  }
};

class StatusLED {
  private:
    int pinRed;
    int pinGreen;
    int pinBlue;
    int pos;
    unsigned long nextUpdate;
    Vector<LedStep> steps;
    void reset();

  public:
    StatusLED(int pinRed, int pinGreen, int pinBlue);
    void setSteps(Vector<LedStep> steps);
    void update();
    void RGB(int redValue, int greenValue, int blueValue);
    void esperar();
    void preparar();
    void funcionar();
};

#endif
