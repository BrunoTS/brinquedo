#ifndef LASER_H
#define LASER_H
#include <Arduino.h>
class Laser {
  
  private:
    byte pin;
    void init();
    
  public:
    Laser(byte pin);
    void on();
    void off();
};
#endif
